#include "StdAfx.h"
#include "DUIObjEditor.h"             

DUIObjEditor::DUIObjEditor()
{
	m_pShow		 = NULL;
	m_pObjTree	 = NULL;
	m_pHoverlFrame= NULL;
	m_pAlignmentFrame = NULL;
	m_pDragFrame = NULL;
	m_bInit      = false;
	m_pReferenceLine1 = NULL;
	m_pReferenceLine2 = NULL;

	m_DesignMod = FixedMode/*NoneMode*/;
}

DMCode DUIObjEditor::InitObjEditor()
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		g_pDMApp->CreateRegObj((void**)&m_pDuiPos, DUIPos::GetClassNameW(), DMREG_Window);
		m_pObjTree = g_pMainWnd->FindChildByNameT<ED::DUIEffectTreeCtrl>(EFFECTTREENAME); DMASSERT(m_pObjTree);
		m_pHoverlFrame = g_pMainWnd->FindChildByNameT<DUIStatic>(L"ds_hoverframe");DMASSERT(m_pHoverlFrame);
		m_pAlignmentFrame = g_pMainWnd->FindChildByNameT<DUIAlignmentFrame>(L"ds_alignmentframe"); DMASSERT(m_pAlignmentFrame);
		m_pDragFrame   = g_pMainWnd->FindChildByNameT<DUIDragFrame>(L"ds_dragframe");DMASSERT(m_pDragFrame);
		m_pReferenceLine1 = g_pMainWnd->FindChildByNameT<DUIReferenceLine>(L"ds_referenceLine1"); DMASSERT(m_pReferenceLine1);
		m_pReferenceLine2 = g_pMainWnd->FindChildByNameT<DUIReferenceLine>(L"ds_referenceLine2"); DMASSERT(m_pReferenceLine2);
		SetDesignMode((DesignMode)SelectMode);
		m_bInit = true;
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
} 

DMCode DUIObjEditor::UnInitObjEditor()
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (false == m_bInit)
		{
			break;
		}

		m_pDuiPos->UnInitLayout();
		m_pDragFrame->UnlinkDragMeta();
		RemoveAllDesignChild();
		m_pHoverlFrame->DM_SetVisible(false,true);
		m_pAlignmentFrame->DM_SetVisible(false, true);
		m_pDragFrame->DM_SetVisible(false,true);
		m_pReferenceLine1->DM_SetVisible(false, true);
		m_pReferenceLine2->DM_SetVisible(false, true);
		SetRangeSize(CSize(0,0));
		m_pShow		 = NULL;
		m_DesignMod = SelectMode;
		DM_Invalidate();
		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode DUIObjEditor::SetDesignMode(DesignMode ds_mode)
{
	// 1.默认隐藏Hover Frame
	m_pHoverlFrame->DM_SetVisible(false,true);
	m_pAlignmentFrame->DM_SetVisible(false, true);
	m_pReferenceLine1->DM_SetVisible(false, true);
	m_pReferenceLine2->DM_SetVisible(false, true);
	m_DesignMod = ds_mode;
	if (m_pShow)
	{
		DragFrameInSelMode();
		ReferenceLineFrameInSelMode();
		m_pShow->SetDesignMode(m_DesignMod);
	}
	return DM_ECODE_OK;
}

DUIRoot* DUIObjEditor::InitDesignChild(HDMTREEITEM hRootTree)
{
	if (hRootTree == NULL)
		return NULL;
	ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(hRootTree);
	DUIRoot* pRootWnd = pData ? pData->m_pRootWnd : NULL;
	if (!pRootWnd)
	{
		g_pDMApp->CreateRegObj((void**)&pRootWnd, DUIRoot::GetClassName(), DMREG_Window);
		OnScroll(true, SB_THUMBPOSITION, 0);
		DM_InsertChild(pRootWnd);
		pRootWnd->m_pParent = this;
		pRootWnd->InitDesignEditor(hRootTree);
		pRootWnd->DM_SetVisible(false, true);
		m_pAlignmentFrame->DM_SetWndToTop();
		m_pHoverlFrame->DM_SetWndToTop();
		m_pDragFrame->DM_SetWndToTop();
		m_pReferenceLine1->DM_SetWndToTop();
		m_pReferenceLine2->DM_SetWndToTop();
	}
	return pRootWnd;
}

DMCode DUIObjEditor::UnlinkTreeChildNode(HDMTREEITEM hItem)
{
	if (hItem == NULL)
		return DM_ECODE_FAIL;
	ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(hItem);
	DMASSERT(pData && pData->m_pRootWnd);
	if (pData && pData->m_pRootWnd)
	{
		m_pDragFrame->UnlinkDragMeta();
		m_pHoverlFrame->DM_SetVisible(false, true);
		m_pAlignmentFrame->DM_SetVisible(false, true);
		m_pDragFrame->DM_SetVisible(false, true);
		m_pReferenceLine1->DM_SetVisible(false, true);
		m_pReferenceLine2->DM_SetVisible(false, true);
		if (pData->m_pRootWnd == pData->m_pDUIWnd)
		{
			DM_DestroyChildWnd(pData->m_pRootWnd);
			m_pShow = NULL;
			UpdateScrollRangeSize();
		}
		else
			pData->m_pRootWnd->DM_DestroyChildWnd(pData->m_pDUIWnd);
		DM_Invalidate();
	}

	return DM_ECODE_OK;
}

DUIRoot* DUIObjEditor::GetShowDesignChild()
{
	if (m_pShow)
		return m_pShow;
	DUIWindow *pChild = m_Node.m_pFirstChild;
	while (pChild)
	{
		if (pChild->DM_IsVisible()&&0 == _wcsicmp(pChild->V_GetClassName(),DUIRoot::GetClassName()))
		{
			break;
		}
		pChild = pChild->m_Node.m_pNextSibling;
	}
	return (DUIRoot*)pChild;
}

DMCode DUIObjEditor::ShowDesignChild(DUIRoot* pShow)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (pShow == m_pShow)
		{
			break;
		}
		if (m_pShow)
		{
			m_pShow->m_pCurSeleDUIWnd = NULL;//old的选择当前选中设置为空
			m_pShow->DM_SetVisible(false,true);
		}
		m_pShow = pShow;
		if (m_pShow)
		{
			m_pShow->DM_SetVisible(true,true);
		}

		DV_UpdateChildLayout();
		UpdateScrollRangeSize();
		if(m_pShow) m_pShow->DM_UpdateLayout(NULL);
		SetDesignMode(m_DesignMod);

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode DUIObjEditor::RemoveAllDesignChild()
{
	DUIWindow*pChild = DM_GetWindow(GDW_FIRSTCHILD);
	while (pChild)
	{
		DUIWindow* pTemp = pChild;
		pChild = pChild->DM_GetWindow(GDW_NEXTSIBLING);
		if (0 == _wcsicmp(pTemp->V_GetClassName(),DUIRoot::GetClassName()))
		{
			DM_DestroyChildWnd(pTemp);
		}
	}
	m_pShow = NULL;
	return true;
}

void DUIObjEditor::UpdateScrollRangeSize()
{
	// 使用可见的ObjDesignWnd来设置滚动条
	DUIWindow *pChild = GetShowDesignChild();
	if (pChild)
	{
		CSize szView(0/*ROOTPNGSIZEX*/, ROOTPNGSIZEY + 2 * ROOTWNDYOFFSET);
		SetRangeSize(szView);
		int nTargetY = m_rcWindow.top - pChild->m_rcWindow.top + ROOTWNDYOFFSET - ROOTPNGYOFFSET;
		DMAutoResetT<bool> bAutoMute(&m_bUseRange, false);
		SetScrollPos(true, nTargetY, true);//OnScroll(true, SB_THUMBPOSITION, nTargetY);
		m_ptCurPos.y = nTargetY;
	}
	else
	{
		SetRangeSize(CSize(0, 0));
	}
} 

void DUIObjEditor::OnRangeCurPosChanged(CPoint ptOld,CPoint ptNew)
{
	DUIWindow *pChild = GetShowDesignChild();
	if (pChild)
	{
		CRect rcChild = pChild->m_rcWindow;
		rcChild.OffsetRect(ptOld-ptNew);
		pChild->DM_FloatLayout(rcChild);
		SetDesignMode(m_DesignMod);
	}
}

DMCode DUIObjEditor::DV_UpdateChildLayout()
{
	DMCode iRet = __super::DV_UpdateChildLayout();
	if (m_pShow)
	{
		CRect rcWindow = m_pShow->m_rcWindow;
		rcWindow.MoveToX(-m_pShow->m_szWndInitSizeX / 2 + m_rcWindow.left + m_rcWindow.Width() / 2);
		m_pShow->DM_FloatLayout(rcWindow);
		DragFrameInSelMode();
		ReferenceLineFrameInSelMode();
		m_pShow->DM_Invalidate();
	}
	return iRet;
}

DMCode DUIObjEditor::HoverInSelMode(DUIWindow* pDUIHover)
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{ 
		if (FixedMode == m_DesignMod || NULL == pDUIHover)
		{
			break;
		}
		CRect rcFrame = pDUIHover->m_rcWindow;

		rcFrame.InflateRect(1,1,1,1);
		m_pHoverlFrame->SetAttribute(L"skin",L"ds_hoverframe");
		
		m_pHoverlFrame->DM_FloatLayout(rcFrame);
		m_pHoverlFrame->DM_SetVisible(true,true);
		DM_Invalidate();

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode DUIObjEditor::AlignMentFrameInSelMode(DUIWindow* pDUIHover, AlignmentType alignType)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (FixedMode == m_DesignMod || NULL == pDUIHover)
		{
			break;
		}
		CRect rcFrame = pDUIHover->m_rcWindow;

		CRect rtRoot;
		m_pShow->DV_GetClientRect(rtRoot);		
		rtRoot.OffsetRect(ROOTPNGXOFFSET, ROOTPNGYOFFSET);
		rtRoot.right = rtRoot.left + ROOTPNGSIZEX;
		rtRoot.bottom = rtRoot.top + ROOTPNGSIZEY;
		m_pAlignmentFrame->SetRootWndRect(rtRoot);
		m_pAlignmentFrame->SetAlignmentType(alignType);
		m_pAlignmentFrame->SetAttribute(L"skin", L"ds_alignmentframe");

		rcFrame.InflateRect(1, 1, 1, 1);
		m_pAlignmentFrame->DM_FloatLayout(rcFrame);
		m_pAlignmentFrame->DM_SetVisible(true, true);
		DM_Invalidate();

		iErr = DM_ECODE_OK;
	} while (false);
	return iErr;
}

DMCode DUIObjEditor::HideHoverFrameWnd()
{
	if (m_pObjTree->m_hHoverItem)
	{
		HDMTREEITEM hHoverItem = m_pObjTree->m_hHoverItem;
		m_pObjTree->m_hHoverItem = NULL;
		m_pObjTree->RedrawItem(hHoverItem);
	}
	m_pHoverlFrame->DM_SetVisible(false, true);
	return DM_ECODE_OK;
}

DMCode DUIObjEditor::HideAlignMentFrameWnd()
{
	if (m_pAlignmentFrame->DM_IsVisible())
	{
		m_pAlignmentFrame->DM_SetVisible(false, true);
		DM_Invalidate();
	}
	return DM_ECODE_OK;
}

DMCode DUIObjEditor::HideDragFrameWnd()
{
	m_pDragFrame->DM_SetVisible(false, true);
	DM_Invalidate();
	return DM_ECODE_OK;
}

DMCode DUIObjEditor::HideReferenceLineFrameWnd()
{
	m_pReferenceLine1->DM_SetVisible(false, true);
	m_pReferenceLine2->DM_SetVisible(false, true);
	return DM_ECODE_OK;
}

DMCode DUIObjEditor::DragFrameInSelMode()
{
	DMCode iErr = DM_ECODE_FAIL;
	do 
	{
		if (SelectMode != m_DesignMod || NULL == g_pMainWnd || NULL == m_pObjTree || NULL == g_pMainWnd->m_hObjSel)
		{
			break;
		}
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(m_pObjTree->GetSelectedItem());
		if (!pData || !pData->IsValid())
		{
			break;
		}
		
		CRect rcFrame;
		if (!DMSUCCEEDED(m_pDragFrame->InitDragFrame(pData,rcFrame)))
		{
			break;
		}
		m_pDragFrame->DM_FloatLayout(rcFrame);
		m_pDragFrame->DM_SetVisible(true,true);
		DM_Invalidate();
		iErr = DM_ECODE_OK;
	} while (false);
	if (!DMSUCCEEDED(iErr))
	{
		m_pDragFrame->DM_SetVisible(false,true);
	}
	return iErr;
}

DMCode DUIObjEditor::ReferenceLineFrameInSelMode()
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (SelectMode != m_DesignMod || NULL == g_pMainWnd || NULL == m_pObjTree || NULL == g_pMainWnd->m_hObjSel)
		{
			break;
		}
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pObjTree->GetItemData(m_pObjTree->GetSelectedItem());
		if (!pData || !pData->IsValid() || !pData->m_pJsonParser)
		{
			break;
		}

		if (0 != _wcsicmp(pData->m_pJsonParser->V_GetClassName(), EDPartsNodeParser::GetClassName()))//只有2D贴纸才有参考线
			break;

		EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pData->m_pJsonParser);
		if (!pPartsNodeParser)
			break;

		CPoint pt1 = { -1, -1 }, pt2 = { -1, -1 };
		pPartsNodeParser->GetReferencePoint(pt1, pt2);
		
		CRect rcFrame = pData->m_pDUIWnd->m_rcWindow;
		CRect rtRoot;
		pData->m_pRootWnd->DV_GetClientRect(rtRoot);

		if (pt1.x != -1 && pt1.y != -1)
		{
			CRect rcLine1;
			rcLine1.TopLeft() = rcFrame.CenterPoint();
			pt1.Offset(rtRoot.left + ROOTPNGXOFFSET, rtRoot.top + ROOTPNGYOFFSET);
			rcLine1.BottomRight() = pt1;
			rcLine1.NormalizeRect();

			rcLine1.InflateRect(3, 3, 3, 3);
			m_pReferenceLine1->SetDotPoint(pData->m_pDUIWnd->m_rcWindow.CenterPoint(), pt1);
			m_pReferenceLine1->DM_FloatLayout(rcLine1);
			m_pReferenceLine1->DM_SetVisible(true, true);
		}
		else
			m_pReferenceLine1->DM_SetVisible(false, true);

		if (pt2.x != -1 && pt2.y != -1)
		{
			CRect rcLine2;
			rcLine2.TopLeft() = rcFrame.CenterPoint();
			pt2.Offset(rtRoot.left + ROOTPNGXOFFSET, rtRoot.top + ROOTPNGYOFFSET);
			rcLine2.BottomRight() = pt2;
			rcLine2.NormalizeRect();

			rcLine2.InflateRect(3, 3, 3, 3);
			m_pReferenceLine2->SetDotPoint(pData->m_pDUIWnd->m_rcWindow.CenterPoint(), pt2);
			m_pReferenceLine2->DM_FloatLayout(rcLine2);
			m_pReferenceLine2->DM_SetVisible(true, true);
		}
		else
			m_pReferenceLine2->DM_SetVisible(false, true);

		DM_Invalidate();
		iErr = DM_ECODE_OK;
	} while (false);
	if (!DMSUCCEEDED(iErr))
	{
		m_pReferenceLine1->DM_SetVisible(false, true);
		m_pReferenceLine2->DM_SetVisible(false, true);
	}
	return iErr;
}

