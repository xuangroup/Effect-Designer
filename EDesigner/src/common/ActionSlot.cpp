#include "StdAfx.h"
#include "ActionSlot.h"

ActionSlot::ActionSlot()
{
}

ActionSlot::~ActionSlot()
{
}

NullActionSlot::NullActionSlot()
{
}

NullActionSlot::~NullActionSlot()
{
}

DMCode NullActionSlot::PerformUndoActionSlot()
{
	return DM_ECODE_FAIL;
}

DMCode NullActionSlot::PerformRedoActionSlot()
{
	return DM_ECODE_FAIL;
}

TreeSelectActionSlot::TreeSelectActionSlot(HDMTREEITEM hOldItem, HDMTREEITEM hNewItem, ED::DUIEffectTreeCtrl* pTree)
{
	m_hOldItem			= hOldItem;
	m_pTreeOldDataLaw	= NULL;
	m_pTreeOldlParam	= NULL;
	m_hNewItem			= hNewItem;
	m_pTreeNewDataLaw	= NULL;
	m_pTreeNewlParam	= NULL;
	m_pTreeCtrl = pTree;

	if (NULL != m_hOldItem)
	{
		DM::LPTVITEMEX Data = m_pTreeCtrl->GetItem(m_hOldItem);
		m_pTreeOldDataLaw = Data;
		if (m_pTreeOldDataLaw) m_pTreeOldlParam = m_pTreeOldDataLaw->lParam;
	}
	if (NULL != m_hNewItem)
	{
		DM::LPTVITEMEX Data = m_pTreeCtrl->GetItem(m_hNewItem);
		m_pTreeNewDataLaw = Data;
		if (m_pTreeNewDataLaw) m_pTreeNewlParam = m_pTreeNewDataLaw->lParam;
	}
}

TreeSelectActionSlot::~TreeSelectActionSlot()
{
}

DMCode TreeSelectActionSlot::PerformUndoActionSlot()
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTreeCtrl)
		{
			DMASSERT(FALSE);
			break;
		}

		if (m_hOldItem == m_hNewItem) //相同的Item  认为是无效SelectChange动作
			break;

		if (m_hOldItem == NULL)
		{
			m_pTreeCtrl->SelectItem(NULL);
			iRet = DM_ECODE_OK;
			break;
		}

		if (!m_pTreeCtrl->IsItemStillExist(m_pTreeOldDataLaw, m_pTreeOldlParam))
		{
			break;
		}
		m_pTreeCtrl->SelectItem(m_hOldItem);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode TreeSelectActionSlot::PerformRedoActionSlot()
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_pTreeCtrl)
		{
			DMASSERT(FALSE);
			break;
		}

		if (m_hOldItem == m_hNewItem) //相同的Item  认为是无效SelectChange动作
			break;

		if (m_hNewItem == NULL)
		{
			m_pTreeCtrl->SelectItem(NULL);
			iRet = DM_ECODE_OK;
			break;
		}

		if (!m_pTreeCtrl->IsItemStillExist(m_pTreeNewDataLaw, m_pTreeNewlParam))
		{
			break;
		}
		m_pTreeCtrl->SelectItem(m_hNewItem);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

TreeDeleteActionSlot::TreeDeleteActionSlot(HDMTREEITEM hItem, ED::DUIEffectTreeCtrl* pTreeCtrl)
{
	m_pTreeCtrl				= pTreeCtrl;
	m_hParentItem			= NULL;
	m_pTreeParentDataLaw	= NULL;
	m_pTreeParentlParam		= NULL;
	m_hChildItem			= NULL;
	m_pTreeChildDataLaw		= NULL;
	m_pTreeChildlParam		= NULL;
	
	if (!m_pTreeCtrl->GetParentItem(hItem)) //删除父节点
	{
		m_bDeleteParentItem = true;
		HDMTREEITEM hItemChild = m_pTreeCtrl->GetChildItem(hItem);
		while (hItemChild)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pTreeCtrl->GetItemData(hItemChild); DMASSERT(pData);
			if (pData)
			{
				//m_vtNods.push_back(pData->m_pDocNodeData);
			}
			hItemChild = m_pTreeCtrl->GetNextSiblingItem(hItemChild);
		}
	}
	else //删除子项
	{
		m_bDeleteParentItem = false;
		m_hParentItem = m_pTreeCtrl->GetParentItem(hItem);
		m_pTreeParentDataLaw = m_pTreeCtrl->GetItem(m_hParentItem);
		if (m_pTreeParentDataLaw) m_pTreeParentlParam = m_pTreeParentDataLaw->lParam;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (pData)
		{
			//m_vtNods.push_back(pData->m_pDocNodeData);
		}
	}
}

TreeDeleteActionSlot::~TreeDeleteActionSlot()
{
}

DMCode TreeDeleteActionSlot::PerformUndoActionSlot()//重新添加回来
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (m_bDeleteParentItem)
		{
			HDMTREEITEM hItem = NULL;// = g_pMainWnd->add2DStickerMajor(L"2DSticker", EFFECTTEMPBTN_2DSTICKERMAJOR); // todo: 支持 makeups;
			m_hParentItem = hItem;
			m_pTreeParentDataLaw = m_pTreeCtrl->GetItem(m_hParentItem);
			if (m_pTreeParentDataLaw) m_pTreeParentlParam = m_pTreeParentDataLaw->lParam;
			//EDCORE::BLDocument& doc = g_pCoreApp->activeDoc();

			//for (size_t i = 0; i < m_vtNods.size(); i++)
			//{
				//doc.addStickerNode2last(m_vtNods[i]);
				//std::shared_ptr<EDCORE::BLItemNode> pitemNode = std::dynamic_pointer_cast<EDCORE::BLItemNode>(m_vtNods[i]);
				//g_pMainWnd->AddEffectNodeItem(hItem, pitemNode->getName().c_str(), pitemNode->enable(), false, false, m_vtNods[i]);
			//}

			iRet = DM_ECODE_OK;
		}
		else //删除的是子节点
		{
			//std::shared_ptr<EDCORE::BLNode> pNode = m_vtNods[0];
			//EDCORE::BLDocument& doc = g_pCoreApp->activeDoc();

			//doc.addStickerNode2last(pNode);
			//std::shared_ptr<EDCORE::BLItemNode> pitemNode = std::dynamic_pointer_cast<EDCORE::BLItemNode>(pNode);
			if (m_hParentItem && m_pTreeCtrl->IsItemStillExist(m_pTreeParentDataLaw, m_pTreeParentlParam))
			{
// 				HDMTREEITEM hItemNode = g_pMainWnd->AddEffectNodeItem(m_hParentItem, pitemNode->getName().c_str(), pitemNode->enable(), false, false, pNode);
// 				m_hChildItem = hItemNode;
// 				m_pTreeChildDataLaw = m_pTreeCtrl->GetItem(m_hChildItem);
// 				if (m_pTreeChildDataLaw) m_pTreeChildlParam = m_pTreeChildDataLaw->lParam;
				iRet = DM_ECODE_OK;
			}
			else
			{
// 				HDMTREEITEM hItemNode = g_pMainWnd->AddEffectNodeItem(m_pTreeCtrl->GetRootItem(), pitemNode->getName().c_str(), pitemNode->enable(), false, false, pNode);
// 				m_hChildItem = hItemNode;
// 				m_pTreeChildDataLaw = m_pTreeCtrl->GetItem(m_hChildItem);
// 				if (m_pTreeChildDataLaw) m_pTreeChildlParam = m_pTreeChildDataLaw->lParam;
				iRet = DM_ECODE_OK;
			}
		}
	} while (false);
	return iRet;
}

DMCode TreeDeleteActionSlot::PerformRedoActionSlot()//重新删除
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (m_bDeleteParentItem)
		{
			if (m_hParentItem && m_pTreeCtrl->IsItemStillExist(m_pTreeParentDataLaw, m_pTreeParentlParam))
			{
				g_pMainWnd->m_pObjEditor->UnlinkTreeChildNode(m_hParentItem);
				m_pTreeCtrl->RemoveItem(m_hParentItem);
				m_hParentItem = NULL;
				m_pTreeParentDataLaw = NULL;
				m_pTreeParentlParam = NULL;
				iRet = DM_ECODE_OK;
			}
		}
		else //删除的是子节点
		{
			if (m_hChildItem && m_pTreeCtrl->IsItemStillExist(m_pTreeChildDataLaw, m_pTreeChildlParam))
			{
				g_pMainWnd->m_pObjEditor->UnlinkTreeChildNode(m_hChildItem);
				m_pTreeCtrl->RemoveItem(m_hChildItem);
				m_hChildItem = NULL;
				m_pTreeChildDataLaw = NULL;
				m_pTreeChildlParam = NULL;
				iRet = DM_ECODE_OK;
			}
		}
	} while (false);
	return iRet;
}

TreeAddActionSlot::TreeAddActionSlot(HDMTREEITEM hItem, ED::DUIEffectTreeCtrl* pTreeCtrl)
{
	m_pTreeCtrl				= pTreeCtrl;
	m_pTreeParentDataLaw	= NULL;
	m_pTreeParentParam		= NULL;
	m_pTreeChildDataLaw		= NULL;
	m_pTreeChildlParam		= NULL;
	m_hChildItem			= NULL;
	m_hParentItem			= NULL;

	if (!m_pTreeCtrl->GetParentItem(hItem)) //添加父节点
	{
		m_bAddParentItem = true;
		m_hParentItem = hItem;
		m_pTreeParentDataLaw = m_pTreeCtrl->GetItem(m_hParentItem);
		if (m_pTreeParentDataLaw) m_pTreeParentParam = m_pTreeParentDataLaw->lParam;
		HDMTREEITEM hItemChild = m_pTreeCtrl->GetChildItem(hItem);
		while (hItemChild)
		{
			ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pTreeCtrl->GetItemData(hItemChild); DMASSERT(pData);
			if (pData)
			{
				//m_vtNods.push_back(pData->m_pDocNodeData);
			}
			hItemChild = m_pTreeCtrl->GetNextSiblingItem(hItemChild);
		}
	}
	else //没有子项
	{
		m_bAddParentItem = false;
		m_hParentItem = m_pTreeCtrl->GetParentItem(hItem);
		m_pTreeParentDataLaw = m_pTreeCtrl->GetItem(m_hParentItem);
		if (m_pTreeParentDataLaw) m_pTreeParentParam = m_pTreeParentDataLaw->lParam;
		m_hChildItem = hItem;
		m_pTreeChildDataLaw = m_pTreeCtrl->GetItem(m_hChildItem);
		if (m_pTreeChildDataLaw) m_pTreeChildlParam = m_pTreeChildDataLaw->lParam;
		ObjTreeDataPtr pData = (ObjTreeDataPtr)m_pTreeCtrl->GetItemData(hItem); DMASSERT(pData);
		if (pData)
		{
			//m_vtNods.push_back(pData->m_pDocNodeData);
		}
	}
}

TreeAddActionSlot::~TreeAddActionSlot()
{
}

DMCode TreeAddActionSlot::PerformUndoActionSlot() //删除新加的
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (m_bAddParentItem)
		{
			if (m_hParentItem && m_pTreeCtrl->IsItemStillExist(m_pTreeParentDataLaw, m_pTreeParentParam))
			{
				g_pMainWnd->m_pObjEditor->UnlinkTreeChildNode(m_hParentItem);
				m_pTreeCtrl->RemoveItem(m_hParentItem);
				m_hParentItem = NULL;
				m_pTreeParentDataLaw = NULL;
				iRet = DM_ECODE_OK;
			}
		}
		else //添加的是子节点
		{
			if (m_hChildItem && m_pTreeCtrl->IsItemStillExist(m_pTreeChildDataLaw, m_pTreeChildlParam))
			{
				g_pMainWnd->m_pObjEditor->UnlinkTreeChildNode(m_hChildItem);
				m_pTreeCtrl->RemoveItem(m_hChildItem);
				m_hChildItem = NULL;
				m_pTreeChildDataLaw = NULL;
				m_pTreeChildlParam = NULL;
				iRet = DM_ECODE_OK;
			}
		}
	} while (false);	
	return iRet;
}

DMCode TreeAddActionSlot::PerformRedoActionSlot()//添加回新的
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (m_bAddParentItem)
		{
			HDMTREEITEM hItem = NULL;// = g_pMainWnd->add2DStickerMajor(L"2DSticker", EFFECTTEMPBTN_2DSTICKERMAJOR);
			m_hParentItem = hItem;
			m_pTreeParentDataLaw = m_pTreeCtrl->GetItem(m_hParentItem);
			if (m_pTreeParentDataLaw) m_pTreeParentParam = m_pTreeParentDataLaw->lParam;
			//EDCORE::BLDocument& doc = g_pCoreApp->activeDoc();
			//for (size_t i = 0; i < m_vtNods.size(); i++)
			//{
				//std::shared_ptr<EDCORE::BLItemNode> pitemNode = std::dynamic_pointer_cast<EDCORE::BLItemNode>(m_vtNods[i]);
				//doc.addStickerNode2last(pitemNode);
				//g_pMainWnd->AddEffectNodeItem(hItem, pitemNode->getName().c_str(), pitemNode->enable(), false, false, pitemNode);
			//}
			iRet = DM_ECODE_OK;
		}
		else //添加的是子节点
		{
			//std::shared_ptr<EDCORE::BLItemNode> pitemNode = std::dynamic_pointer_cast<EDCORE::BLItemNode>(m_vtNods[0]);
			//EDCORE::BLDocument& doc = g_pCoreApp->activeDoc();

			//doc.addStickerNode2last(pitemNode);
			if (m_hParentItem && m_pTreeCtrl->IsItemStillExist(m_pTreeParentDataLaw, m_pTreeParentParam))
			{
				//HDMTREEITEM hItemNode = g_pMainWnd->AddEffectNodeItem(m_hParentItem, pitemNode->getName().c_str(), pitemNode->enable(), false, false, pitemNode);
// 				m_hChildItem = hItemNode;
// 				m_pTreeChildDataLaw = m_pTreeCtrl->GetItem(m_hChildItem);
// 				if (m_pTreeChildDataLaw) m_pTreeChildlParam = m_pTreeChildDataLaw->lParam;
				iRet = DM_ECODE_OK;
			}
			else
			{
// 				HDMTREEITEM hItemNode = g_pMainWnd->AddEffectNodeItem(m_pTreeCtrl->GetRootItem(), pitemNode->getName().c_str(), pitemNode->enable(), false, false, pitemNode);
// 				m_hChildItem = hItemNode;
// 				m_pTreeChildDataLaw = m_pTreeCtrl->GetItem(m_hChildItem);
// 				if (m_pTreeChildDataLaw) m_pTreeChildlParam = m_pTreeChildDataLaw->lParam;
				iRet = DM_ECODE_OK;
			}
		}
	} while (false);
	return iRet;
}

ComboSeleChangeActionSlot::ComboSeleChangeActionSlot(HDMTREEITEM hItem, int iOldSelect, int iNewSelect, DUIComboBox*pCombobox, ED::DUIEffectTreeCtrl* pTreeCtrl)
{
	m_iOldSelect	= iOldSelect; 
	m_iNewSelect	= iNewSelect;
	m_pComboBox		= pCombobox;
	m_pTreeCtrl		= pTreeCtrl;
	m_hItem			= hItem;
	m_pTreelParam	= NULL;
	m_pTreeDataLaw	= NULL;
	if (NULL != m_hItem)
	{
		DM::LPTVITEMEX Data = m_pTreeCtrl->GetItem(m_hItem);
		m_pTreeDataLaw = Data;
		if (m_pTreeDataLaw) m_pTreelParam = m_pTreeDataLaw->lParam;
	}
	DMASSERT(m_pComboBox);
}

ComboSeleChangeActionSlot::~ComboSeleChangeActionSlot()
{
}

DMCode ComboSeleChangeActionSlot::PerformUndoActionSlot()
{
	DMCode iRet = DM_ECODE_FAIL;
	do 
	{
		if (!m_hItem)
			break;
		if (m_iOldSelect == m_iNewSelect) //相同的项选择  认为是无效动作
			break;
		if (!m_pTreeCtrl->IsItemStillExist(m_pTreeDataLaw, m_pTreelParam))
			break;

		DMAutoResetT<bool> bAutoMute(&g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot(), true);
		if (m_pTreeCtrl->GetSelectedItem() != m_hItem)
		{
			m_pTreeCtrl->SelectItem(m_hItem);
		}
		m_pComboBox->SetCurSel(m_iOldSelect);
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode ComboSeleChangeActionSlot::PerformRedoActionSlot()
{
	if (m_iOldSelect == m_iNewSelect) //相同的项选择  认为是无效动作
		return DM_ECODE_FAIL;
	DMAutoResetT<bool> bAutoMute(&g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot(), true);
	if (m_pTreeCtrl->GetSelectedItem() != m_hItem)
	{
		m_pTreeCtrl->SelectItem(m_hItem);
	}
	m_pComboBox->SetCurSel(m_iNewSelect);
	return DM_ECODE_OK;
}

EditorElemPosChgActionSlot::EditorElemPosChgActionSlot(HDMTREEITEM hItem, CRect OldRect, CRect NewRect, ED::DUIEffectTreeCtrl* pTreeCtrl, DUIDragFrame* pDragFrame)
{
	m_hItem			= hItem;
	m_pTreeCtrl		= pTreeCtrl;
	m_pDragFrame	= pDragFrame;
	m_pTreeParam	= NULL;
	m_OldRect		= OldRect;
	m_NewRect		= NewRect;
	m_pTreeDataLaw	= NULL;
	if (m_hItem)
	{
		m_pTreeDataLaw = m_pTreeCtrl->GetItem(m_hItem);
		if (m_pTreeDataLaw) m_pTreeParam = m_pTreeDataLaw->lParam;
	}
}

EditorElemPosChgActionSlot::~EditorElemPosChgActionSlot()
{
}

DMCode EditorElemPosChgActionSlot::PerformUndoActionSlot()
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_hItem || !m_pTreeCtrl->IsItemStillExist(m_pTreeDataLaw, m_pTreeParam))
			break;
		if (!m_pTreeCtrl->SelectItem(m_hItem))
			break;

		DMAutoResetT<bool> bAutoMute(&g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot(), true);
		m_pDragFrame->m_StartDragRc = m_NewRect;
		m_pDragFrame->m_dragMetas[0].m_Rect = m_OldRect;
		m_pDragFrame->SetElementRect(m_OldRect);
		m_pDragFrame->OnLButtonUp(0, 0);		
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}

DMCode EditorElemPosChgActionSlot::PerformRedoActionSlot()
{
	DMCode iRet = DM_ECODE_FAIL;
	do
	{
		if (!m_hItem || !m_pTreeCtrl->IsItemStillExist(m_pTreeDataLaw, m_pTreeParam))
			break;
		if (!m_pTreeCtrl->SelectItem(m_hItem))
			break;

		DMAutoResetT<bool> bAutoMute(&g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot(), true);
		m_pDragFrame->m_StartDragRc = m_OldRect;
		m_pDragFrame->m_dragMetas[0].m_Rect = m_NewRect;
		m_pDragFrame->SetElementRect(m_NewRect);
		m_pDragFrame->OnLButtonUp(0, 0);		
		iRet = DM_ECODE_OK;
	} while (false);
	return iRet;
}