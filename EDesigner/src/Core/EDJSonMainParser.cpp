#include "StdAfx.h"
#include "EDJsonMainParser.h"

// namespace DM
// {
EDJSonMainParser::EDJSonMainParser(IEDJSonParserOwner* pOwner)
{
	InitData();
	m_Node.m_pOwner = pOwner;
}

EDJSonMainParser::~EDJSonMainParser()
{
	DestoryParser();
}

void EDJSonMainParser::InitData()
{
	m_strNewPartField = (L"75.4"), m_dbTotalMem = (3.0), m_strVersion = (L"3.0"), m_strToolVersion = (L"Win_2.2.0");
	m_strDeformationFileName.Empty();
	m_strStickerType.Empty();
	m_bDataDirtyMark = false;
}

DMCode EDJSonMainParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	do
	{
		if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::STRING_deformationFileName))
		{
			if (JsHandleValue.isString())
			{
				m_strDeformationFileName = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::STING_newPartField))
		{
			if (JsHandleValue.isString())
			{
				m_strNewPartField = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::STRING_stickerType))
		{
			if (JsHandleValue.isString())
			{
				m_strStickerType = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::STRING_toolVersion))
		{
			if (JsHandleValue.isString())
			{
				m_strToolVersion = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::DOUBLE_totalMem))
		{
			if (JsHandleValue.isDouble())
			{
				m_dbTotalMem = JsHandleValue.toDouble();
				iErr = DM_ECODE_OK;
			}
			else if (JsHandleValue.isInt())
			{
				m_dbTotalMem = (double)JsHandleValue.toInt();
				iErr = DM_ECODE_OK;
			}
		}
		else if (0 == _stricmp(pszAttribute, EDAttr::EDJSonMainParserAttr::STRING_version))
		{
			if (JsHandleValue.isString())
			{
				m_strVersion = JsHandleValue.toStringW();
				iErr = DM_ECODE_OK;
			}
		}
	} while (false);
	if (!bLoadJSon)
		MarkDataDirty();
	return iErr;
}

DMCode EDJSonMainParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	do
	{
		GenerateStickerTypeInfoValue();
		if (!m_strDeformationFileName.IsEmpty())
		{
			JSonHandler[EDAttr::EDJSonMainParserAttr::STRING_deformationFileName].putStringW(m_strDeformationFileName);
		}
		JSonHandler[EDAttr::EDJSonMainParserAttr::STING_newPartField].putStringW(m_strNewPartField);
		JSonHandler[EDAttr::EDJSonMainParserAttr::STRING_stickerType].putStringW(m_strStickerType);
		JSonHandler[EDAttr::EDJSonMainParserAttr::STRING_toolVersion].putStringW(m_strToolVersion);
		JSonHandler[EDAttr::EDJSonMainParserAttr::DOUBLE_totalMem].putDouble(m_dbTotalMem);
		JSonHandler[EDAttr::EDJSonMainParserAttr::STRING_version].putStringW(m_strVersion);
	} while (false);
	return DM_ECODE_OK;
}

void EDJSonMainParser::GenerateStickerTypeInfoValue()
{
	m_strStickerType.Empty();
	if (FindChildParserByClassName(EDPartsNodeParser::GetClassNameW())) //2D��ֽ
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"2D");
	}
	if (FindChildParserByClassName(EDBackgroundEdgeParser::GetClassNameW()))
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"backgroundEdge");
	}
	if (FindChildParserByClassName(EDFaceMorphParser::GetClassNameW()))
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"faceMorph");
	}
	if (FindChildParserByClassName(EDBeautifyParser::GetClassNameW()))
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"beautify");
	}
	if (FindChildParserByClassName(EDFaceExchangeParser::GetClassNameW()))
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"faceExchange");
	}
	if (!m_strDeformationFileName.IsEmpty())
	{
		m_strStickerType += (m_strStickerType.IsEmpty() ? L"" : L"_") + CStringW(L"faceDeformation");
	}
}

void EDJSonMainParser::FreeParser(EDJSonParser* pParser)
{//do nothing only reset data
	InitData();
	RemoveAll();//remove unparser json node
}

//}; //end namespace DM