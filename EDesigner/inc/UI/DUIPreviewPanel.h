// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIPreviewPanel.h
// File mark:   
// File summary:实时预览所在的区域  为了响应DV_UpdateChildLayout来调整实时预览窗口的位置
// Author:		
// Edition:     1.0
// Create date: 2019-4-9
// ----------------------------------------------------------------

#pragma once

class DUIPreviewPanel : public DUIWindow
{
	DMDECLARE_CLASS_NAME(DUIPreviewPanel, DUINAME_PREVIEWPANEL, DMREG_Window)

public:
	DUIPreviewPanel();
	~DUIPreviewPanel();

	virtual DMCode DV_UpdateChildLayout();

private:

};

