//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDBeautifyPartsParser.h 
// File Des: Beautify字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-6-9	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDBeautifyParserAtrr
	{
	public:
		static char* DOUBLE_contrastStrength;				///< 示例:"contrastStrength": 0.05
		static char* DOUBLE_enlargeRatio;					///< 示例:"enlargeRatio": 0.13
		static char* DOUBLE_reddenStrength;					///< 示例:"reddenStrength": 0.36
		static char* DOUBLE_saturation;						///< 示例:"saturation": 0.5
		static char* DOUBLE_shrinkRatio;					///< 示例:"shrinkRatio": 0.1
		static char* DOUBLE_smallRatio;						///< 示例:"smallRatio":0.1
		static char* DOUBLE_smoothStrength;					///< 示例:"smoothStrength": 0.74
		static char* DOUBLE_whitenStrength;					///< 示例:"whitenStrength": 0.3		
	};
	EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_contrastStrength)EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_enlargeRatio)
	EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_reddenStrength)EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_saturation)
	EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_shrinkRatio)EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_smallRatio)
	EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_smoothStrength)EDAttrValueInit(EDBeautifyParserAtrr, DOUBLE_whitenStrength)
}

/// <summary>
///		"Beautify"节点json字段解析类
/// </summary>
class EDBeautifyPartsParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDBeautifyPartsParser, L"beautifyParts", DMREG_Attribute);
public:
	EDBeautifyPartsParser();
	~EDBeautifyPartsParser();

	DMCode BuildJSonData(JSHandle JSonHandler) override;
};

/// <summary>
///		"Beautify"每个子节点json字段解析类
/// </summary>
class EDBeautifyParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDBeautifyParser, L"beautify", DMREG_Attribute);
public:
	EDBeautifyParser();
	~EDBeautifyParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

public:
	double m_dbEnlargeRatio;
	double m_dbShrinkRatio;
	double m_dbSmallRatio;
	double m_dbReddenStrength;
	double m_dbWhitenStrength;
	double m_dbSmoothStrength;
	double m_dbContrastStrength;
	double m_dbSaturation;
};