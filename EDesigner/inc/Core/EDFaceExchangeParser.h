//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDFaceExchangeParser.h 
// File Des: FaceExchange字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-6-7	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDEDFaceExchangeParserAtrr
	{
	public:
		static char* INT_maxFaceSupported;					///< 示例:"maxFaceSupported": 2
		static char* INT_zPosition;							///< 示例:"INT_zPosition" : 0
	};
	EDAttrValueInit(EDEDFaceExchangeParserAtrr, INT_maxFaceSupported)
	EDAttrValueInit(EDEDFaceExchangeParserAtrr, INT_zPosition)
}

/// <summary>
///		"FaceExchange"节点json字段解析类
/// </summary>
class EDFaceExchangeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDFaceExchangeParser, L"faceExchange", DMREG_Attribute);
public:
	EDFaceExchangeParser();
	~EDFaceExchangeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

	int GetZPositionOrder() override;
	bool SetZPositionOrder(int iZPostion) override;

public:
	int m_iMaxFaceSupported;
	int m_iZPosition;
};