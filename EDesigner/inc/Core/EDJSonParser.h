//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDJSonParser.h 
// File Des:Json解析类的基类  json解析类从这里继承
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-24	1.0		
//-------------------------------------------------------
#pragma once

/// <summary>
///		json文件字段解析基类传递接口 
/// </summary>
class IEDJSonParserOwner
{
public:
	virtual DMCode NewJsonParserCreatedNotify(EDJSonParser* pJSonParser) = 0;			///< 解析JSon文件新的节点生成通知
	virtual CStringW JSonParserGetJSonFilePath() = 0;									///< 获取解析JSon文件路径回调
	virtual DMCode ParseJSNodeObjFinished(EDJSonParser* pJSonParser) = 0;				///< 加载文件节点解析完成
	virtual DMCode RelativeResourceNodeParserChgNotify(EDJSonParser* pJSonParser) = 0;	///< 节点的资源绑定的通知
	virtual DMCode ResourceNodeOwnerParserChgNotify(EDJSonParser* pJSonParser) = 0;		///< 资源节点拥有者改变的通知
	virtual DMCode JSonParserDataDirtyNodtify(EDJSonParser* pJSonParser) = 0;			///< 数据脏通知
	virtual DMCode JsonParserOnFreeNotify(EDJSonParser* pJSonParser) = 0;				///< JSon节点被删除
};
/// <summary>
///		json文件字段解析基类  "faceMorph" "makeups”解析类都从这里继承
/// </summary>
class EDResourceNodeParser;
class EDJSonParser : public EDBase, public DMMapT<CStringA, JSHandle>
{
	EDDECLARE_CLASS_NAME(EDJSonParser, L"EDJSonParser", DMREG_Attribute);
public:
	EDJSonParser();
	virtual ~EDJSonParser();

	////// 下面是加载json文件
	DMCode LoadJSonFile(CStringW strJSonPath);															///< 解析JSon文件 序列化过程
	DMCode InitJSonData(JSHandle &JSonHandler) override;												///< 解析JSon
	DMCode ParseMemberJSObj(LPCSTR pszAttribute, JSHandle& JSonHandler, bool bLoadJSon) override;		///< 解析JSon 子节点成员的变量
	DMCode ParseJSNodeObjFinished() override;															///< 完成整个node的解析
	DMCode SetJSonMemberKey(LPCSTR lpKeyVal) override;													///< 节点的key的值
	void MarkDataDirtyRecursive(bool bDirty);															///< 递归设置脏位
	bool RecursiveCheckIsDataDirty();																	///< 递归查询脏位
	virtual void MarkDataDirty(bool bDirty = true);														///< 设置脏位
	virtual DMCode SetJSonAttributeInt(LPCSTR pszAttribute, int iVal);									///< 外部设置Int类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonAttributeBool(LPCSTR pszAttribute, bool bVale);								///< 外部设置boll类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonAttributeDouble(LPCSTR pszAttribute, double dbVale);							///< 外部设置double类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonAttributeString(LPCSTR pszAttribute, LPCSTR lpVale);							///< 外部设置string类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonAttributeString(LPCSTR pszAttribute, LPCWSTR lpVale);							///< 外部设置wstring类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonMemberAttributeInt(LPCSTR pszMemberName, LPCSTR pszAttribute, int iVale);		///< 外部设置member中的int类型数据回json文档 会设置脏数据
	virtual DMCode SetJSonArrayAttributeInt(LPCSTR pszAttribute, const CArray<int>& array);				///< 外部设置Array的int类型数据回json文档 会设置脏数据
	DMCode InsertChildParser(EDJSonParser* pNewChild, EDJSonParser* pInsertAfter = JSPARSER_LAST);		///< 插入子节点 维护节点树形结构
	DMCode MoveParserItemToNewPos(EDJSonParser* pInsertAfter = JSPARSER_LAST);							///< 同级间移动位置
	EDJSonParserPtr CreateChildParser(CStringW strChildClassName, EDJSonParser* pInsertAfter = JSPARSER_LAST);   ///< 插入子节点 维护节点树形结构
	EDJSonParser* GetParser(int iCode);																	///< 获取指定的JSonParser
	IEDJSonParserOwner* GetParserOwner();																///< 获取owner
	virtual DMCode RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon = false);				///< 关联Resource指针
	virtual DMCode GetRelativeResourceImgSize(INT& uiWidth, INT& uiHeight);								///< 获取关联资源图片的大小
	virtual EDResourceNodeParser* GetRelativeResourceNodeParser();										///< 获取绑定的资源
	virtual double GetParserImageScale();																///< 获取ImageScale
	virtual void SetParserItemRect(const CRect& rect);													///< 设置矩形区域  这里会设置position  会设置脏数据
	virtual CPoint GetParserItemTopLeftPt();															///< 获取Item左上角
	virtual int  GetZPositionOrder();																	///< 获取zposition
	virtual bool SetZPositionOrder(int iZPostion);														///< 设置zposition 成功表示节点有zposition  否则没有zposition
	virtual bool IsParserEnable();																		///< 获取m_bEnable
	EDJSonParser* GetTopParentParser();																	///< 获取顶层的JSonParser
	EDJSonParser* FindChildParserByClassName(CStringW strClassName);									///< 通过class name 获取JSonParser的子节点
	DMCode RemoveChildParser(EDJSonParser* pChild);														///< 移除子节点 解除关系而已
	DMCode DestoryChildParser(EDJSonParser* pChild);													///< 彻底释放子节点
	DMCode DestoryParser();																				///< 彻底释放节点
	virtual void FreeParser(EDJSonParser* pParser);														///< delete节点

	////// 下面是生成json文件
	DMCode BuildJSonFile(CStringW strJSonPath);															///< 生成JSon文件 反序列化过程
	DMCode BuildJSonData(JSHandle JSonHandler) override;												///< 递归生成Json节点的入口
	virtual DMCode BuildUnParserNode(JSHandle &JSonHandler);											///< 生成没有解析的字段
public:
	CStringA			m_strJSonMemberKey;																///< "faceMorph": { 中的“faceMorph”  节点的key
	bool				m_bDataDirtyMark;																///< 脏位
	EDJSonParserNode	m_Node;																			///< jsonparser树形列表
	HDMTREEITEM			m_hTreeBindItem;																///< 绑定树的句柄

private:
	static JSObject m_JSobject;																			///< 必须是成员变量不能是局部变量 因为需要保存UnParserNode
};
