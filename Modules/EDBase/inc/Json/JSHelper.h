// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	JSHelper.h
// File mark:   
// File summary:基于原大厅的封装代码
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-2-22
// ----------------------------------------------------------------
#pragma once

namespace Json{ class Value;};
// -------------------------------------------------------------------------
class JSObject;

/// <summary>
///		JsonHandle 代表json对象引用
/// </summary>
class ED_EXPORT JSHandle
{
public:
	enum JsonType
	{
		JsonNull = 0,	 ///< 'null' value
		JsonInt,		 ///< signed integer value
		JsonUInt,		 ///< unsigned integer value
		JsonDouble,		 ///< double value
		JsonString,		 ///< UTF-8 string value
		JsonBool,		 ///< bool value
		JsonArray,		 ///< array value (ordered list)
		JsonObject,		 ///< object value (collection of name/value pairs).
		JsonByte = 10,
		JsonInt64,		 ///< signed integer value
		JsonUInt64,      ///< unsigned integer value
	};
	explicit JSHandle();
	JSHandle(const JSHandle& src);
	~JSHandle();

	bool isValid() const;
	bool isEqual(const JSHandle& other) const;
	JsonType getType() const;											///<	返回Json类型
	bool operator!() const;
	bool operator ==(const JSHandle& other) const;
	bool operator !=(const JSHandle& other) const;

public:
	JSHandle& operator=(const JSHandle& src);
	JSHandle& operator=(bool bValue);
	JSHandle& operator=(int iValue);
	JSHandle& operator=(unsigned int uValue);
	JSHandle& operator=(signed __int64 i64Value);
	JSHandle& operator=(unsigned __int64 u64Value);
	JSHandle& operator=(double dValue);
	JSHandle& operator=(LPCSTR pszValue);
	JSHandle& operator=(LPCWSTR pszValue);

public:
	bool putBool(bool data);
	bool putInt(int data);
	bool putUInt(unsigned __int32 data);
	bool putInt64(signed __int64 data);
	bool putUInt64(unsigned __int64 data);
	bool putDouble(double data);
	bool putString(LPCSTR data);
	bool putStringW(LPCWSTR data);
	bool putUTF8String(const char* data);
	bool putBytes(const void* data, int size);
	bool getBytes(unsigned char* pbOut, int& nSize);

public:
	bool isNull() const;
	bool isBool() const;
	bool isInt() const;
	bool isUInt() const;
	bool isInt64() const;
	bool isDouble() const;
	bool isString() const;
	bool isArray() const;
	bool isObject() const;
	bool isBytes() const;

public:
	bool empty() const;													///< 空数组，空对象，空值返回true
public:
	LPCSTR		toStringA() const;
	LPCWSTR     toStringW() const;
	int toInt() const;
	unsigned int toUInt() const;
	signed __int64 toInt64() const;
	unsigned __int64 toUInt64() const;
	double toDouble() const;
	bool toBool() const;
	bool toBytes(unsigned char* pBuffer, int& nSize) const;

public:
	void swap(JSHandle& other);										///< 交换

	/// ------------------------------------
	/// @brief	Remove all object members and array elements. 
	/// @remark	
	/// @return	[void]
	void clear();

	/// ------------------------------------
	/// @brief	Resize the array to size elements. New elements are initialized to null.May only be called on nullValue or arrayValue. 
	/// @param	[int] size
	/// @remark	
	/// @return	[void]
	void resize(int size);

	/// -------------------------------------
	/// @brief	返回数组结点个数。
	/// @remark	
	/// @return	[int]
	int arraySize() const;

	/// -------------------------------------
	/// @brief	Access an array element (zero based index ).
	/// @param	[int] index
	/// @remark	If the array contains less than index element, then null value are inserted
	///         in the array so that its size is index+1. (You may need to say 'value[0u]' to get your compiler to distinguish
	///         this from the operator[] which takes a string.)
	/// @return	[JsonHandle]
	JSHandle operator[](int index);

	/// -------------------------------------
	/// @brief	Append value to array at the end. 
	/// @param	[const JsonHandle &] value
	/// @remark	Equivalent to jsonvalue[jsonvalue.size()] = value;
	/// @return	[JsonHandle]
	JSHandle append(const JSHandle& value);

	/// -------------------------------------
	/// @brief	Access an object value by name, create a null member if it does not exist.
	/// @param	[const char *] key
	/// @remark	
	/// @return	[JsonHandle]
	JSHandle operator[](const char *key);



	/// -------------------------------------
	/// @brief  Access an object value by name, create a null member if it does not exist.
	/// @param	[const wchar_t *] key
	/// @remark	
	/// @return	[JsonHandle]
	JSHandle operator[](const wchar_t *key);


	/// -------------------------------------
	/// @brief	删除相应key
	/// @param	[const char *] key
	/// @remark	
	/// @return	[JsonObject]
	JSObject remove(const char* key);

	/// -------------------------------------
	/// @brief	是否存在key
	/// @param	[const char *] key
	/// @remark	
	/// @return	[bool]
	bool hasKey(const char *key) const;

	std::vector<std::string> memberNames() const;
protected:
	Json::Value*			m_jsonValue;
};

/// <summary>
///		JsonObject 代表json对象实体。
/// </summary>
class ED_EXPORT JSObject: public JSHandle
{
public:
	JSObject(JsonType jsonType = JsonNull);
	JSObject(bool data);
	JSObject(int data);
	JSObject(unsigned int data);
	JSObject(double data);
	JSObject(const char* data);
	JSObject(const char* beginValue, const char *endValue);
	~JSObject();
	JSObject(const JSObject& src);

	/// -------------------------------------
	/// @brief	（这里约定）当JsonHandle赋值给JsonObject是进行深复制
	/// @param	[const JsonHandle &] src
	/// @remark	
	/// @return	[]
	JSObject(const JSHandle& src);
	JSObject& operator=(const JSObject& src);

	/// -------------------------------------
	/// @brief	（这里约定）当JsonHandle赋值给JsonObject是进行深复制。
	/// @param	[const JsonHandle &] src
	/// @remark	
	/// @return	[JsonObject&]
	JSObject& operator=(const JSHandle& src);

public:
	bool jsonWrite(std::string* josnStringUTF8String);
	bool jsonWrite(std::wstring* josnStringW);
	bool jsonParse(const char* beginDoc, const char* endDoc = NULL);
	bool jsonParse(LPCWSTR beginDoc, LPCWSTR endDoc = NULL);
	bool jsonParse(const std::string& jsonString);
};
