// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	UrlHelper.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-9
// ----------------------------------------------------------------
#pragma once

namespace UrlHelper
{
	ED_EXPORT bool IsValidUrl(LPCWSTR lpszUrl);///< 检验指定的URL是否是有效的URL
	ED_EXPORT bool IsDownloadUrl(LPCWSTR lpszUrl);///< 检验指定的URL是否是下载URL
	ED_EXPORT bool SetCookieForMainDomain(LPCWSTR pszCookie);
	ED_EXPORT bool SetCookieForUrl(LPCWSTR lpszUrl, LPCWSTR lpszCookie);///< 对指定URl设置COOKIE
	ED_EXPORT bool IsPageTitleValid(LPCWSTR lpszTitle);///< 检验是否是有效的页面标题
	ED_EXPORT bool FormatUrl(std::wstring& strRedirect,std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassport, std::wstring& strRso,std::wstring& strRef,DMOUT std::wstring& strGameUrl);///< 拼成url地址
	ED_EXPORT std::wstring FormatUrlTags(std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassPort);///< 拼成UrlTags,唯一标识一个url,
	ED_EXPORT bool UnFormatUrlTags(std::wstring& strTags,std::wstring& strGameId, std::wstring& strServerId, std::wstring& strPassPort);///< 反向，由strTags分解成strGameId+strServerId+strPassPort
	ED_EXPORT std::wstring GetDomainForUrl(std::wstring strUrl);
}