@echo off

set batDir=%~dp0
shift & set OutDir=%1

cd /d %batDir%

echo lzlong clear  output start------------------------------------
cd /d %OutDir%
@del /f /s /q *.aps
@del /f /s /q *.exp
echo lzlong clear  output end------------------------------------

echo lzlong copy dbghelp.dll start-----------------------------------
cd /d %batDir%
copy /y .\dbghelp.dll %OutDir%\dbghelp.dll
xcopy /d /y /s ..\7z %OutDir%\7z\
xcopy /d /y /s ..\makeupres %OutDir%\makeupres\

rem robocopy . %OutDir% dbghelp.dll
echo lzlong copy dbghelp.dll end-----------------------------------

